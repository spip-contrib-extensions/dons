<?php
/**
 * Utilisations de pipelines par Dons
 *
 * @plugin     Dons
 * @copyright  2021
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Dons\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Optimiser la base de données
 *
 * Supprime les liens orphelins de l'objet vers quelqu'un et de quelqu'un vers l'objet.
 * Supprime les objets à la poubelle.
 * Supprime les objets à la poubelle.
 *
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function dons_optimiser_base_disparus($flux) {

	include_spip('action/editer_liens');
	$flux['data'] += objet_optimiser_liens(array('dons_campagne'=>'*'), '*');

	sql_delete('spip_dons_campagnes', "statut='poubelle' AND maj < " . $flux['args']['date']);

	sql_delete('spip_dons', "statut='poubelle' AND maj < " . $flux['args']['date']);

	return $flux;
}

/**
 * Créer la commande finale à la fin des inscriptions ou modifs de profils
 * 
 * @pipeline formulaire_traiter
 * @param array $flux
 * 		Flux du pipeline contenant toutes les saisies des formulaires
 * @return array
 * 		Retourne le flux possiblement modifié
 **/
function dons_formulaire_traiter($flux) {
	$formulaires = pipeline(
		'commandes_generer_apres_formulaires',
		array('editer_auteur', 'inscription', 'profil')
	);
	
	if (
		is_array($formulaires)
		and in_array($flux['args']['form'], $formulaires)
		and $id_auteur = $flux['data']['id_auteur']
	) {
		$flux['data'] += dons_generer_commande($id_auteur);
		
		// On ajoute la référence de la commande créée si on part sur une autre page
		if (
			isset($flux['data']['redirect'])
			and $flux['data']['redirect']
			and isset($flux['data']['reference'])
			and $flux['data']['reference']
		) {
			$flux['data']['redirect'] = parametre_url($flux['data']['redirect'], 'reference', $flux['data']['reference']);
		}
	}
	
	return $flux;
}

/**
 * Générer la commande suivant ce qu'on a gardé en session
 * 
 * @param int id_auteur
 * 		Identifiant de l'utilisateur qui commande
 * @return array
 * 		Retourne un tableau des retours de création des objets Commande et Transaction
 **/
function dons_generer_commande($id_auteur) {
	include_spip('inc/session');
	include_spip('base/abstract_sql');
	include_spip('action/editer_objet');
	include_spip('action/editer_commande');
	
	$retours = array();
	$commande_don = session_get('commande_don');
	
	// Si on trouve des infos de commande de don en session
	if (
		$id_auteur = intval($id_auteur)
		and is_array($commande_don)
		and $id_dons_campagne = intval($commande_don['id_dons_campagne'])
		and $montant = floatval($commande_don['montant'])
	) {
		$frequence = $commande_don['frequence'];
		$type_don = $commande_don['type_don'];
		$periodicite = '';
		$echeances = array();
		$montant_ht = $montant;
		$campagne = sql_fetsel('*', 'spip_dons_campagnes', 'id_dons_campagne ='.$id_dons_campagne);
		
		if (in_array($frequence, array('mois', 'annee'))) {
			$periodicite = $frequence;
			
			$echeances = array(
				array('montant_ht' => $montant_ht, 'montant' => $montant),
			);
		}

		// Il ne peut y avoir qu'une seule commande en cours à la fois :
		// s'il y a déjà une commande en cours précédente, on la supprime.
		// Cf. creer_commande_encours() dans le plugin commandes.
		if (($id_commande = intval(session_get('id_commande'))) > 0) {
			// Si la commande est toujours "encours" il faut la mettre à la poubelle
			// il ne faut pas la supprimer tant qu'il n'y a pas de nouvelles commandes pour etre sur qu'on reutilise pas son numero
			// (sous sqlite la nouvelle commande reprend le numero de l'ancienne si on fait delete+insert)
			if (
				$statut = sql_getfetsel('statut', 'spip_commandes', 'id_commande = ' . intval($id_commande))
				and $statut == 'encours'
			) {
				spip_log("Commande ancienne encours->poubelle en session : $id_commande", 'commandes_abonnements');
				sql_updateq('spip_commandes', array('statut' => 'poubelle'), 'id_commande = ' . intval($id_commande));
			}
			// Dans tous les cas on supprime la valeur de session
			session_set('id_commande');
		}

		// On crée une nouvelle commande, le don ne sera créé ou renouvelé que lors du paiement !
		if (
			$id_commande = commande_inserer(0, array(
				'id_auteur' => $id_auteur,
				'echeances_type' => $periodicite,
				'echeances' => $echeances,
				'source' => 'donscampagne#' .$id_dons_campagne,
			))
		) {
			include_spip('inc/filtres');
			$titre_campagne = generer_info_entite($id_dons_campagne, 'dons_campagne', 'titre');
			
			// On remplit la commande avec la campagne demandée
			if ($id_commandes_detail = objet_inserer('commandes_detail', 0, array(
				'id_commande' => $id_commande,
				'descriptif' => $titre_campagne,
				'infos_extras' => json_encode(array('type_don' => $type_don)),
				'objet' => 'dons_campagne',
				'id_objet' => $id_dons_campagne,
				'quantite' => 1,
				'prix_unitaire_ht' => $montant_ht,
			))) {
				// On retourne la référence et l'id
				$retours['id_commande'] = $id_commande;
				$retours['reference'] = sql_getfetsel(
					'reference',
					'spip_commandes',
					'id_commande=' . intval($id_commande)
				);
				// Et on supprime le pseudo-panier de la session, puis on met la vraie commande
				session_set('commande_don', null);
				session_set('id_commande', $id_commande);
			}
		}
	}
	
	return $retours;
}


/*
 * Des modifs supplémentaires après édition
 */

function dons_post_edition($flux) {
	if (empty($flux['args']['table'])) {
		return $flux;
	}
	
	// Si on modifie un abonnement
	if ($flux['args']['table'] == 'spip_dons') {
		$id_don = intval($flux['args']['id_objet']);
		//~ $don = sql_fetsel('*', 'spip_dons', 'id_don = ' . $id_don);
		//~ $campagne = sql_fetsel('*', 'spip_dons_campagnes', 'id_dons_campagne = ' . intval($don['id_dons_campagne']));
		//~ $jourdhui = date('Y-m-d H:i:s');

		// Si on a un id_commande dans l'environnement, on lie la commande à l'abonnement
		if (
				$id_commande = intval(_request('id_commande'))
				and defined('_DIR_PLUGIN_COMMANDES')
		) {
			// On lie cet abonnement avec la commande qui l'a généré
			include_spip('action/editer_liens');
			objet_associer(
				array('commande' => $id_commande), array('don' => $id_don)
			);
		}
	}
	// Détection magique du plugin Commandes et d'une commande de don
	elseif (
	// Si on institue une commande
			$flux['args']['table'] == 'spip_commandes'
			and $id_commande = intval($flux['args']['id_objet'])
			and $flux['args']['action'] == 'instituer'
			// Et qu'on passe en statut "paye" depuis autre chose
			and $flux['data']['statut'] == 'paye'
			and $flux['args']['statut_ancien'] != 'paye'
			// Et que la commande existe bien
			and $commande = sql_fetsel('*', 'spip_commandes', 'id_commande = ' . $id_commande)
			// Et que cette commande a un utilisateur correct
			and ($id_auteur = $commande['id_auteur']) > 0
			// Et qu'on a des détails dans cette commande
			and $details = sql_allfetsel('*', 'spip_commandes_details', 'id_commande = ' . $id_commande)
			and is_array($details)
	) {
		include_spip('inc/autoriser');
		include_spip('action/editer_objet');
		
		// On cherche si on a des campagnes de don dans les détails de la commande
		foreach ($details as $detail) {
			// Si on trouve un don à faire
			if ($detail['objet'] == 'dons_campagne' and ( $id_dons_campagne = $detail['id_objet']) > 0) {
				// Si la commande est renouvelable et que c'est le PREMIER paiement (activation)
				// on force toujours la création d'un nouveau don
				$forcer_creation = false;
				if (
						in_array($commande['echeances_type'], array('mois', 'annee'))
						and include_spip('inc/commandes_echeances')
						and commandes_nb_echeances_payees($id_commande) <= 1
				) {
					$forcer_creation = true;
				}

				// On garde l'id_commande dans l'environnement (pour que lorsqu'un nouveau don est créé ensuite, ça fasse le lien)
				set_request('id_commande', $id_commande);
				
				// On crée ou complète le don
								
				// Si on trouve déjà un don existant de cette commande récurrente
				if (
					// Si c'est pas un premier paiement
					!$forcer_creation
					// Tous les dons déjà liés à cette commande
					and $dons_existants = sql_allfetsel('id_objet', 'spip_commandes_liens', array('objet = "don"', 'id_commande = '.$id_commande))
					and $dons_existants = array_map('reset', $dons_existants)
					// Le don lié mais de la bonne campagne
					and $don_existant = sql_fetsel('*', 'spip_dons', array('id_dons_campagne = '.$id_dons_campagne, sql_in('id_don', $dons_existants)))
				) {
					autoriser_exception('modifier', 'don', $don_existant['id_don']);
					autoriser_exception('instituer', 'don', $don_existant['id_don']);
					
					// Nouveau montant total
					$montant_total = $don_existant['montant_total'] + $detail['prix_unitaire_ht'];
					
					// On met à jour
					objet_modifier(
						'don',
						$don_existant['id_don'],
						array(
							'montant_total' => $montant_total,
							'statut' => 'publie',
						)
					);
				}
				// Sinon on crée un nouveau don
				else {
					if ($id_don = objet_inserer('don')) {
						autoriser_exception('modifier', 'don', $id_don);
						autoriser_exception('instituer', 'don', $id_don);
						
						$type_don = '';
						if (
							$detail['infos_extras']
							and $infos_extras = json_decode($detail['infos_extras'], true)
							and isset($infos_extras['type_don'])
						) {
							$type_don = $infos_extras['type_don'];
						}
						
						objet_modifier(
							'don',
							$id_don,
							array(
								'id_dons_campagne' => $id_dons_campagne,
								'id_auteur' => $id_auteur,
								'montant' => $detail['prix_unitaire_ht'],
								'montant_total' => $detail['prix_unitaire_ht'],
								'frequence' => $commande['echeances_type'] ? $commande['echeances_type'] : 'une',
								'type_don' => $type_don,
								'date' => date('Y-m-d H:i:s'), // la date de la première fois toujours
								'statut' => 'publie',
							)
						);
					}
				}
				//~ $retour = abonnements_creer_ou_renouveler($id_auteur, $id_abonnements_offre, $forcer_creation);
			}
		}
	}
	
	return $flux;
}
