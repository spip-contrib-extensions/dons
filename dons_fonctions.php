<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cherche le niveau actuel d'avancement de la campagne suivant l'objectif
 */
function dons_niveau_actuel($id_dons_campagne) {
	$niveau_actuel = 0;
	$id_dons_campagne = intval($id_dons_campagne);
	
	if ($campagne = sql_fetsel('objectif,objectif_niveau_initial', 'spip_dons_campagnes', 'id_dons_campagne = '.$id_dons_campagne)) {
		$niveau_actuel += $campagne['objectif_niveau_initial'];
		
		if ($campagne['objectif'] == 'montant') {
			$niveau_actuel += sql_getfetsel('sum(montant_total)', 'spip_dons', 'id_dons_campagne = '.$id_dons_campagne);
		}
		elseif ($campagne['objectif'] == 'nb') {
			$niveau_actuel += sql_getfetsel('count(id_auteur)', 'spip_dons', 'id_dons_campagne = '.$id_dons_campagne, 'id_auteur');
		}
	}
	
	return $niveau_actuel;
}
