<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'dons_description' => 'Ce plugin permet de créer des campagnes de dons et de proposer aux gens de donner à telle ou telle campagne avec le plugin Bank.',
	'dons_nom' => 'Dons',
	'dons_slogan' => 'Gérer des dons et leur paiement',
);
