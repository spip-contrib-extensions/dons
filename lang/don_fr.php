<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_don' => 'Ajouter ce don',

	// C
	'champ_choix_montant_label' => 'Je donne',
	'champ_choix_montant_annee_label' => 'Je donne annuellement',
	'champ_choix_montant_mois_label' => 'Je donne mensuellement',
	'champ_choix_montant_une_label' => 'Je donne une fois',
	'champ_date_label' => 'Date',
	'champ_frequence_label' => 'Fréquence',
	'champ_id_auteur_label' => 'Personne donatrice',
	'champ_type_don_label' => 'Type de don',
	'champ_type_don_choix_organisation_label' => 'Organisation',
	'champ_type_don_choix_particulier_label' => 'Particulier',
	'champ_id_dons_campagne_label' => 'Campagne de dons',
	'champ_montant_label' => 'Montant',
	'champ_montant_libre_label' => 'Montant libre',
	'champ_montant_total_label' => 'Montant total',
	'confirmer_supprimer_don' => 'Confirmez-vous la suppression de ce don ?',

	// I
	'icone_creer_don' => 'Créer un don',
	'icone_modifier_don' => 'Modifier ce don',
	'info_1_don' => 'Un don',
	'info_aucun_don' => 'Aucun don',
	'info_dons_auteur' => 'Les dons de cet auteur',
	'info_nb_dons' => '@nb@ dons',

	// R
	'retirer_lien_don' => 'Retirer ce don',
	'retirer_tous_liens_dons' => 'Retirer tous les dons',

	// S
	'supprimer_don' => 'Supprimer ce don',

	// T
	'texte_ajouter_don' => 'Ajouter un don',
	'texte_changer_statut_don' => 'Ce don est :',
	'texte_creer_associer_don' => 'Créer et associer un don',
	'texte_definir_comme_traduction_don' => 'Ce don est une traduction du dons numéro :',
	'titre_don' => 'Don',
	'titre_dons' => 'Dons',
	'titre_dons_rubrique' => 'Dons de la rubrique',
	'titre_langue_don' => 'Langue de ce don',
	'titre_logo_don' => 'Logo de ce don',
	'titre_objets_lies_don' => 'Liés à ce don',
	'titre_page_dons' => 'Les dons',
);
