<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_dons_campagne' => 'Ajouter cette campagne de dons',

	// C
	'champ_config_objectif_label' => 'Configuration de l’objectif',
	'champ_config_versement_label' => 'Configuration du versement',
	'champ_date_debut_label' => 'Date de début',
	'champ_date_fin_label' => 'Date de fin',
	'champ_descriptif_explication' => 'Un court descriptif percutant.',
	'champ_descriptif_label' => 'Descriptif',
	'champ_fiscalite_label' => 'Fiscalité',
	'champ_fiscalite_choix_fr_label' => 'Française',
	'champ_fiscalite_choix_null_label' => 'Ne pas gérer la fiscalité',
	'champ_fiscalite_explication' => 'Notamment si la campagne est pour une association, vous pouvez gérer des reçus fiscaux suivant le statut particulier ou organisation des personnes donatrices.',
	'champ_frequences_explication' => 'Fréquences des versement proposés (au moins un).',
	'champ_frequences_choix_mois_label' => 'Mensuellement',
	'champ_frequences_choix_annee_label' => 'Annuellement',
	'champ_frequences_choix_une_label' => 'Une fois',
	'champ_frequences_label' => 'Fréquences possibles',
	'champ_montant_libre_nope_explication' => 'Par défaut un champ de montant libre est toujours ajouté.',
	'champ_montant_libre_nope_label_case' => 'Ne pas proposer de montant libre',
	'champ_montant_suggestions_explication' => 'Une suggestion par ligne, de la forme : montant|Label. Par exemple : 500|500 €',
	'champ_montant_suggestions_label' => 'Suggestions de montants',
	'champ_objectif_atteint_cloture_label_case' => 'Ne plus permettre de nouveaux dons une fois l\'objectif atteint.',
	'champ_objectif_atteint_cloture_label' => 'Clôturer la campagne si l\'objectif est atteint',
	'champ_objectif_explication' => 'Fixer un objectif à cette campagne',
	'champ_objectif_choix_aucun_label' => 'Aucun objectif',
	'champ_objectif_choix_montant_label' => 'Montant total',
	'champ_objectif_choix_nb_label' => 'Nombre de personnes donatrices',
	'champ_objectif_label' => 'Objectif',
	'champ_objectif_niveau_initial_explication' => 'Permet d’indiquer le niveau initial de la campagne. Ce champs peut être utilisé pour prendre en compte des dons non compatibilisées par le site.',
	'champ_objectif_niveau_initial_label' => 'Niveau initial',
	'champ_objectif_niveau_explication' => 'Montant ou nombre de personne à atteindre.',
	'champ_objectif_niveau_label' => 'Niveau de l\'objectif',
	'champ_texte_label' => 'Texte',
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_dons_campagne' => 'Confirmez-vous la suppression de cette campagne de dons ?',

	// I
	'icone_creer_dons_campagne' => 'Créer une campagne de dons',
	'icone_modifier_dons_campagne' => 'Modifier cette campagne de dons',
	'info_1_dons_campagne' => 'Une campagne de dons',
	'info_aucun_dons_campagne' => 'Aucune campagne de dons',
	'info_dons_campagnes_auteur' => 'Les campagnes de dons de cet auteur',
	'info_nb_dons_campagnes' => '@nb@ campagnes de dons',
	
	// N
	'noisette_nom' => 'Objectif de campagne de dons',
	'noisette_description' => 'Afficher l’état d’avancement d’une campagne de dons si elle a un objectif configuré.',
	'noisette_id_dons_campagne_choisi_label' => 'Campagne de dons (sinon suivant le contexte)',
	
	// O
	'objectif_atteint' => 'L’objectif de cette campagne a été atteint, merci à vous !',
	'objectif_montant_niveau' => '@niveau_actuel@ collectés sur @objectif@',
	'objectif_nb_niveau' => '@niveau_actuel@ personnes donatrices sur @objectif@',

	// R
	'retirer_lien_dons_campagne' => 'Retirer cette campagne de dons',
	'retirer_tous_liens_dons_campagnes' => 'Retirer toutes les campagnes de dons',

	// S
	'supprimer_dons_campagne' => 'Supprimer cette campagne de dons',

	// T
	'texte_ajouter_dons_campagne' => 'Ajouter une campagne de dons',
	'texte_changer_statut_dons_campagne' => 'Cette campagne de dons est :',
	'texte_creer_associer_dons_campagne' => 'Créer et associer une campagne de dons',
	'texte_definir_comme_traduction_dons_campagne' => 'Cette campagne de dons est une traduction de la campagne de dons numéro :',
	'titre_dons_campagne' => 'Campagne de dons',
	'titre_dons_campagnes' => 'Campagnes de dons',
	'titre_dons_campagnes_rubrique' => 'Campagnes de dons de la rubrique',
	'titre_langue_dons_campagne' => 'Langue de cette campagne de dons',
	'titre_logo_dons_campagne' => 'Logo de cette campagne de dons',
	'titre_objets_lies_dons_campagne' => 'Liés à cette campagne de dons',
	'titre_page_dons_campagnes' => 'Les campagnes de dons',
);
