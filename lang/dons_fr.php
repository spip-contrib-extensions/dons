<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'donner_bouton' => 'Donner',
	'dons_titre' => 'Dons',

	// C
	'cfg_titre_parametrages' => 'Paramétrages',

	// T
	'titre_page_configurer_dons' => 'Dons',
);
