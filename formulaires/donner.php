<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/config');
include_spip('inc/session');

function formulaires_donner_saisies_dist($id_dons_campagne, $retour='') {
	$saisies = array(
		'options' => array(
			'texte_submit' => _T('dons:donner_bouton'),
		),
	);
	
	// Les infos utiles
	$id_dons_campagne = intval($id_dons_campagne);
	$dons_campagne = sql_fetsel('*', 'spip_dons_campagnes', 'id_dons_campagne = '.$id_dons_campagne);
	$frequences = explode(',', $dons_campagne['frequences']);
	
	// Ajout des montants
	$saisies_montant = array();
	// S'il y a des suggestions
	if ($dons_campagne['montant_suggestions'] and $suggestions = saisies_chaine2tableau($dons_campagne['montant_suggestions'])) {
		$saisies_montant[] = array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'montant_suggestion',
				'data' => $suggestions,
			),
		);
	}
	// Si pas désactivé
	if (!$dons_campagne['montant_libre_nope']) {
		$saisies_montant[] = array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'montant_libre',
				'label' => _T('don:champ_montant_libre_label'),
				'size' => 4,
				'placeholder' => '--.--',
			),
			'verifier' => array(
				'type' => 'decimal',
				'options' => array(
					'normaliser' => 'oui',
				),
			),
		);
	}
	
	// Choix de la fréquence
	if (count($frequences) > 1) {
		$data_frequences = array();
		foreach ($frequences as $frequence) {
			$data_frequences[$frequence] = _T("dons_campagne:champ_frequences_choix_{$frequence}_label");
		}
		$saisies_montant[] = array(
			'saisie' => 'radio',
			'options' => array(
				'nom' => 'frequence',
				'data' => $data_frequences,
				'defaut' => $frequences[0],
			),
		);
	}
	
	$saisies[] = array(
		'saisie' => 'fieldset',
		'options' => array(
			'nom' => 'choix_montant',
			'label' => count($frequences) > 1 ? _T('don:champ_choix_montant_label') : _T("don:champ_choix_montant_{$frequences[0]}_label"),
		),
		'saisies' => $saisies_montant,
	);
	
	// Si fiscalité 
	if ($dons_campagne['fiscalite']) {
		$saisies[] = array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'fiscalite',
				'label' => _T('dons_campagne:champ_fiscalite_label'),
			),
			'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'type_don',
						'data' => array(
							'particulier' => _T('don:champ_type_don_choix_particulier_label'),
							'organisation' => _T('don:champ_type_don_choix_organisation_label'),
						),
						'defaut' => 'particulier',
					),
				),
			),
		);
	}
	
	// Le Javascript
	$saisies['options']['inserer_debut'] = '
<link rel="stylesheet" href="'.find_in_path('css/ui/jquery-ui.min.css').'" type="text/css" />
<script type="text/javascript" src="'.find_in_path('prive/javascript/ui/jquery-ui.min.js').'"></script>
<style>.formulaire_donner .choix {display:inline-block; background:transparent; border:0 !important;}</style>
	';
	$saisies['options']['inserer_fin'] = "
<script type=\"text/javascript\">
	/*<![CDATA[*/
	;(function($){
		function dons_js_$id_dons_campagne() {
			// Gestion de la fréquence
			$('.formulaire_donner .editer_frequence').find('input[type=radio]').checkboxradio({icon: false});
			
			// Gestion de la fiscalité
			$('.formulaire_donner .editer_type_don').find('input[type=radio]').checkboxradio({icon: false});
			
			// Gestion des montants persos
			$('.formulaire_donner .editer_montant_suggestion').find('input[type=radio]').checkboxradio({icon: false}).on('focus', function() {
				$(this).closest('.fieldset_choix_montant').find('input[type=text]').removeClass('ui-state-active').val('');
			});
			$('.fieldset_choix_montant input[type=text]')
				.on('focus', function() {
					$(this).addClass('ui-state-active').closest('.fieldset_choix_montant').find('.editer_montant_suggestion input:checked').prop('checked', false).checkboxradio('refresh');
				})
				.on('blur', function() {
					var val = $(this).val().trim();
					if (!val.match(/^[\d\.,]+$/)) {
						$(this).val('').removeClass('ui-state-active').closest('.fieldset_choix_montant').find('.editer_montant_suggestion input[type=radio]').first().prop('checked', true).checkboxradio('refresh');
					}
				});
		}
		
		$(function(){
			dons_js_$id_dons_campagne();
			onAjaxLoad(dons_js_$id_dons_campagne);
		});
	})(jQuery);
	/*]]>*/
	</script>
	";
	
	return $saisies;
}

function formulaires_donner_charger_dist($id_dons_campagne, $retour = '') {
	include_spip('base/objets');
	
	// On teste si on peut donner
	if (!autoriser('donner', 'dons_campagne', $id_dons_campagne)) {
		return array(
			'editable' => false,
			'message_ok' => _T('dons_campagne:objectif_atteint'),
		);
	}
	
	$contexte = array(
		'montant' => '',
	);
	
	return $contexte;
}

function formulaires_donner_verifier_dist($id_dons_campagne, $retour = ''){
	$erreurs = array();
	$dons_campagne = sql_fetsel('*', 'spip_dons_campagnes', 'id_dons_campagne = '.intval($id_dons_campagne));
	$montant_suggestion = _request('montant_suggestion');
	$montant_libre = _request('montant_libre');
	$montant = $montant_libre ? $montant_libre : $montant_suggestion;
	$frequences_possibles = explode(',', $dons_campagne['frequences']);
	if (count($frequences_possibles) > 1 and in_array(_request('frequence'), $frequences_possibles)) {
		$frequence = _request('frequence');
	}
	else {
		$frequence = $frequences_possibles[0];
	}
	
	// Normalement peu de chance d'arriver sauf trafiquage du HTML
	if (!$montant) {
		$erreurs['message_erreur'] = 'Vous devez choisir un montant';
	}
	
	// S'il n'y a aucune erreur, on évite de refaire des tests et on garde le bon montant
	if (!$erreurs) {
		set_request('montant', $montant);
		set_request('frequence', $frequence);
	}
	
	return $erreurs;
}

function formulaires_donner_traiter_dist($id_dons_campagne, $retour = '') {
	include_spip('inc/session');
	$retours = array(
		'redirect' => $retour,
	);
	$dons_campagne = sql_fetsel('*', 'spip_dons_campagnes', 'id_dons_campagne = '.intval($id_dons_campagne));
	$montant = _request('montant');
	$frequence = _request('frequence');
	$type_don = _request('type_don');

	// 1) Soit il y a déjà une commande de don en cours,
	// on la met à jour avec l'offre sélectionnée
	if (
		$id_auteur = session_get('id_auteur')
		and $detail = sql_fetsel(
			'd.*',
			'spip_commandes_details AS d INNER JOIN spip_commandes AS c ON c.id_commande=d.id_commande',
			array(
				'd.objet=' . sql_quote('dons_campagne'),
				'c.statut=' . sql_quote('encours'),
				'c.id_auteur=' . intval($id_auteur),
			)
		)
	) {
		$montant_ht = $montant;
		$echeances = '';
		$periodicite = '';

		// Échéances avec les deux seuls cas qu'on sait gérer pour l'instant
		if ($frequence and in_array($frequence, array('mois', 'annee'))) {
			$periodicite = $frequence;
			$echeances = array(
				array('montant_ht' => $montant_ht, 'montant' => $montant),
			);
			$echeances = serialize($echeances);
		}

		$set_detail = array(
			'descriptif'       => $dons_campagne['titre'],
			'id_objet'         => $id_dons_campagne,
			'prix_unitaire_ht' => $montant_ht,
			'infos_extras'     => json_encode(array('type_don' => $type_don)),
		);
		$set_commande = array(
			'echeances_type' => $periodicite,
			'echeances'      => $echeances,
			'source'         => 'donscampagne#' . $id_dons_campagne,
		);
		sql_updateq('spip_commandes', $set_commande, 'id_commande='.intval($detail['id_commande']));
		sql_updateq('spip_commandes_details', $set_detail, 'id_commandes_detail='.intval($detail['id_commandes_detail']));

		// Puis juste au cas-où, on supprime la session éventuelle
		session_set('commande_don', null);

	// 2) Soit pas de commande, et on enregistre en session les infos nécessaires,
	// elle sera créée quand on aura un utilisateur sous la main
	// et qu'on sera sûr d'avoir ses infos à jour
	} else {
		$commande_don = array(
			'id_dons_campagne' => $id_dons_campagne,
			'montant' => $montant,
			'frequence' => $frequence,
			'type_don' => $type_don,
		);
		session_set('commande_don', $commande_don);
	}
	
	return $retours;
}
