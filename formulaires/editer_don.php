<?php
/**
 * Gestion du formulaire de d'édition de don
 *
 * @plugin     Dons
 * @copyright  2021
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Dons\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Déclaration des saisies de don
 *
 * @param int|string $id_don
 *     Identifiant du don. 'new' pour un nouveau don.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un don source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du don, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_don_saisies_dist($id_don = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$saisies = array(
		array(
			'saisie' => 'dons_campagnes',
			'options' => array(
				'nom' => 'id_dons_campagne',
				 'obligatoire' => 'oui',
				'label' => _T('don:champ_id_dons_campagne_label'),
			),
		),
		array(
			'saisie' => 'auteurs',
			'options' => array(
				'nom' => 'id_auteur',
				'label' => _T('don:champ_id_auteur_label'),
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'montant',
				'label' => _T('don:champ_montant_label'),
			),
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'montant_total',
				'label' => _T('don:champ_montant_total_label'),
			),
		),
		array(
			'saisie' => 'selection',
			'options' => array(
				'nom' => 'type_don',
				'label' => _T('don:champ_type_don_label'),
				'data' => array(
					'particulier' => _T('don:champ_type_don_choix_particulier_label'),
					'organisation' => _T('don:champ_type_don_choix_organisation_label'),
				),
			),
		),
	);
	
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_don
 *     Identifiant du don. 'new' pour un nouveau don.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un don source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du don, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_don_identifier_dist($id_don = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_don)));
}

/**
 * Chargement du formulaire d'édition de don
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_don
 *     Identifiant du don. 'new' pour un nouveau don.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un don source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du don, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_don_charger_dist($id_don = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('don', $id_don, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

	$valeurs['saisies'] = call_user_func_array('formulaires_editer_don_saisies_dist', func_get_args());
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de don
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_don
 *     Identifiant du don. 'new' pour un nouveau don.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un don source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du don, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_don_verifier_dist($id_don = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {

	$erreurs = formulaires_editer_objet_verifier('don', $id_don, array('id_dons_campagne'));


	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de don
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_don
 *     Identifiant du don. 'new' pour un nouveau don.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un don source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du don, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_don_traiter_dist($id_don = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$retours = formulaires_editer_objet_traiter('don', $id_don, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
	return $retours;
}
