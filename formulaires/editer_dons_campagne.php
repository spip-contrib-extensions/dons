<?php
/**
 * Gestion du formulaire de d'édition de dons_campagne
 *
 * @plugin     Dons
 * @copyright  2021
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Dons\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Déclaration des saisies de dons_campagne
 *
 * @param int|string $id_dons_campagne
 *     Identifiant du dons_campagne. 'new' pour un nouveau dons_campagne.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dons_campagne source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dons_campagne, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_dons_campagne_saisies_dist($id_dons_campagne = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$saisies = array(
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'titre',
				'obligatoire' => 'oui',
				'label' => _T('dons_campagne:champ_titre_label'),
			),
		),

		array(
			'saisie' => 'textarea',
			'options' => array(
				'nom' => 'descriptif',
				'label' => _T('dons_campagne:champ_descriptif_label'),
				'explication' => _T('dons_campagne:champ_descriptif_explication'),
				'rows' => 4,
			),
		),

		array(
			'saisie' => 'textarea',
			'options' => array(
				'nom' => 'texte',
				'label' => _T('dons_campagne:champ_texte_label'),
				'rows' => 10,
			),
		),
		
		array(
			'saisie' => 'date',
			'options' => array(
				'nom' => 'date_debut',
				'label' => _T('dons_campagne:champ_date_debut_label'),
				'horaire' => 'oui',
			),
			'verifier' => array(
				'type' => 'date',
				'options' => array(
					'normaliser' => 'datetime',
				),
			),
		),
		array(
			'saisie' => 'date',
			'options' => array(
				'nom' => 'date_fin',
				'label' => _T('dons_campagne:champ_date_fin_label'),
				'horaire' => 'oui',
			),
			'verifier' => array(
				'type' => 'date',
				'options' => array(
					'normaliser' => 'datetime',
				),
			),
		),

		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'config_versement',
				'label' => _T('dons_campagne:champ_config_versement_label'),
			),
			'saisies' => array(
				array(
					'saisie' => 'checkbox',
					'options' => array(
						'nom' => 'frequences',
						'label' => _T('dons_campagne:champ_frequences_label'),
						 'explication' => _T('dons_campagne:champ_frequences_explication'),
						'data' => array(
							'mois' => _T('dons_campagne:champ_frequences_choix_mois_label'),
							'annee' => _T('dons_campagne:champ_frequences_choix_annee_label'),
							'une' => _T('dons_campagne:champ_frequences_choix_une_label'),
						),
						'obligatoire' => 'oui',
					),
				),
				array(
					'saisie' => 'textarea',
					'options' => array(
						'nom' => 'montant_suggestions',
						'label' => _T('dons_campagne:champ_montant_suggestions_label'),
					 'explication' => _T('dons_campagne:champ_montant_suggestions_explication'),
						'rows' => 4,
					),
				),
				array(
					'saisie' => 'case',
					'options' => array(
						'nom' => 'montant_libre_nope',
						'label_case' => _T('dons_campagne:champ_montant_libre_nope_label_case'),
						'explication' => _T('dons_campagne:champ_montant_libre_nope_explication'),
					),
				),
			),
		),
		
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'config_objectif',
				'label' => _T('dons_campagne:champ_config_objectif_label'),
			),
			'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'objectif',
						'label' => _T('dons_campagne:champ_objectif_label'),
						'explication' => _T('dons_campagne:champ_objectif_explication'),
						'data' => array(
							'aucun' => _T('dons_campagne:champ_objectif_choix_aucun_label'),
							'montant' => _T('dons_campagne:champ_objectif_choix_montant_label'),
							'nb' => _T('dons_campagne:champ_objectif_choix_nb_label'),
						),
						'defaut' => 'aucun',
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'objectif_niveau',
						'label' => _T('dons_campagne:champ_objectif_niveau_label'),
						'explication' => _T('dons_campagne:champ_objectif_niveau_explication'),
						'afficher_si' => '@objectif@ != "aucun"',
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'objectif_niveau_initial',
						'label' => _T('dons_campagne:champ_objectif_niveau_initial_label'),
						'explication' => _T('dons_campagne:champ_objectif_niveau_initial_explication'),
						'afficher_si' => '@objectif@ != "aucun"',
					),
				),
				array(
					'saisie' => 'case',
					'options' => array(
						'nom' => 'objectif_atteint_cloture',
						'label' => _T('dons_campagne:champ_objectif_atteint_cloture_label'),
						'label_case' => _T('dons_campagne:champ_objectif_atteint_cloture_label_case'),
						'afficher_si' => '@objectif@ != "aucun"',
					),
				),
			),
		),
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'config_fiscalite',
				'label' => _T('dons_campagne:champ_fiscalite_label'),
				'explication' => _T('dons_campagne:champ_fiscalite_explication'),
			),
			'saisies' => array(
				array(
					'saisie' => 'radio',
					'options' => array(
						'nom' => 'fiscalite',
						'data' => array(
							'' => _T('dons_campagne:champ_fiscalite_choix_null_label'),
							'fr' => _T('dons_campagne:champ_fiscalite_choix_fr_label'),
						),
						'defaut' => '',
					),
				),
			),
		),
	);
	
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_dons_campagne
 *     Identifiant du dons_campagne. 'new' pour un nouveau dons_campagne.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dons_campagne source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dons_campagne, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_dons_campagne_identifier_dist($id_dons_campagne = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_dons_campagne)));
}

/**
 * Chargement du formulaire d'édition de dons_campagne
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_dons_campagne
 *     Identifiant du dons_campagne. 'new' pour un nouveau dons_campagne.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dons_campagne source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dons_campagne, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_dons_campagne_charger_dist($id_dons_campagne = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('dons_campagne', $id_dons_campagne, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
	
	// On remet les fréquences en tableau, dans la base en liste à virgules
	if ($valeurs['frequences']) {
		$valeurs['frequences'] = explode(',', $valeurs['frequences']);
	}
	
	$valeurs['saisies'] = call_user_func_array('formulaires_editer_dons_campagne_saisies_dist', func_get_args());
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de dons_campagne
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_dons_campagne
 *     Identifiant du dons_campagne. 'new' pour un nouveau dons_campagne.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dons_campagne source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dons_campagne, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_dons_campagne_verifier_dist($id_dons_campagne = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {

	$erreurs = formulaires_editer_objet_verifier('dons_campagne', $id_dons_campagne, array('titre'));


	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de dons_campagne
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_dons_campagne
 *     Identifiant du dons_campagne. 'new' pour un nouveau dons_campagne.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dons_campagne source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dons_campagne, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_dons_campagne_traiter_dist($id_dons_campagne = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	// On passe les fréquences en liste à virgules
	$frequences = _request('frequences');
	$frequences = implode(',', $frequences);
	set_request('frequences', $frequences);
	
	$retours = formulaires_editer_objet_traiter('dons_campagne', $id_dons_campagne, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
	
	return $retours;
}
