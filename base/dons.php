<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Dons
 * @copyright  2021
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Dons\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function dons_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['dons_campagnes'] = 'dons_campagnes';
	$interfaces['table_des_tables']['dons'] = 'dons';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function dons_declarer_tables_objets_sql($tables) {

	$tables['spip_dons_campagnes'] = array(
		'type' => 'dons_campagne',
		'principale' => 'oui',
		'table_objet_surnoms' => array('donscampagne'), // table_objet('dons_campagne') => 'dons_campagnes' 
		'field'=> array(
			'id_dons_campagne'   => 'bigint(21) NOT NULL',
			'titre'              => 'text NOT NULL DEFAULT ""',
			'descriptif'         => 'text NOT NULL DEFAULT ""',
			'texte'              => 'text NOT NULL DEFAULT ""',
			'date_debut'         => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'date_fin'           => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'frequences'         => 'varchar(255) NOT NULL DEFAULT ""',
			'montant_suggestions' => 'text NOT NULL DEFAULT ""',
			'montant_libre_nope' => 'varchar(25) NOT NULL DEFAULT ""',
			'objectif'           => 'varchar(25) NOT NULL DEFAULT ""',
			'objectif_niveau'    => 'int(11) NOT NULL DEFAULT 0',
			'objectif_niveau_initial' => 'int(11) NOT NULL DEFAULT 0',
			'objectif_atteint_cloture' => 'varchar(25) NOT NULL DEFAULT ""',
			'fiscalite'          => 'varchar(25) NOT NULL DEFAULT ""',
			'statut'             => 'varchar(20)  DEFAULT "0" NOT NULL',
			'maj'                => 'TIMESTAMP NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_dons_campagne',
			'KEY statut'         => 'statut',
		),
		'titre' => 'titre AS titre, "" AS lang',
		 #'date' => '',
		'champs_editables'  => array('titre', 'descriptif', 'texte', 'date_debut', 'date_fin', 'frequences', 'montant_suggestions', 'montant_libre_nope', 'objectif', 'objectif_niveau', 'objectif_niveau_initial', 'objectif_atteint_cloture', 'fiscalite'),
		'champs_versionnes' => array('titre', 'descriptif', 'texte', 'date_debut', 'date_fin', 'frequences', 'montant_suggestions', 'montant_libre_nope', 'objectif', 'objectif_niveau', 'objectif_niveau_initial', 'objectif_atteint_cloture', 'fiscalite'),
		'rechercher_champs' => array("titre" => 10, "descriptif" => 5, "texte" => 5),
		'tables_jointures'  => array('spip_dons_campagnes_liens'),
		'statut_textes_instituer' => array(
			'prepa'    => 'texte_statut_en_cours_redaction',
			'prop'     => 'texte_statut_propose_evaluation',
			'publie'   => 'texte_statut_publie',
			'refuse'   => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prop,prepa',
				//~ 'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'texte_changer_statut' => 'dons_campagne:texte_changer_statut_dons_campagne',


	);

	$tables['spip_dons'] = array(
		'type' => 'don',
		'principale' => 'oui',
		'page' => false,
		'field'=> array(
			'id_don'             => 'bigint(21) NOT NULL',
			'id_dons_campagne'   => 'bigint(21) NOT NULL DEFAULT 0',
			'id_auteur'          => 'bigint(21) NOT NULL DEFAULT 0',
			'montant'            => 'decimal(20,6) not null default 0',
			'montant_total'      => 'decimal(20,6) not null default 0',
			'frequence'          => 'varchar(25) NOT NULL DEFAULT ""',
			'type_don'           => 'varchar(25) NOT NULL DEFAULT ""',
			'date'               => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'statut'             => 'varchar(20)  DEFAULT "0" NOT NULL',
			'maj'                => 'TIMESTAMP NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'          => 'id_don',
			'KEY statut'           => 'statut',
			"KEY id_dons_campagne" => "id_dons_campagne",
			"KEY id_auteur"        => "id_auteur",
		),
		'titre' => 'concat((select titre from spip_dons_campagnes as DC where DC.id_dons_campagne=spip_dons.id_dons_campagne), " - ", (select nom from spip_auteurs as A where A.id_auteur=spip_dons.id_auteur)) AS titre, "" AS lang',
		'date' => 'date',
		'champs_editables'  => array('id_dons_campagne', 'id_auteur', 'montant', 'montant_total', 'frequence', 'type_don', 'date'),
		'champs_versionnes' => array('id_dons_campagne', 'id_auteur', 'montant', 'montant_total', 'frequence', 'type_don', 'date'),
		'rechercher_champs' => array(),
		'tables_jointures'  => array(),
		'join' => array(
			'id_auteur' => 'id_auteur',
			'id_dons_campagne' => 'id_dons_campagne',
		),
		'statut_textes_instituer' => array(
			'prepa'    => 'texte_statut_en_cours_redaction',
			'prop'     => 'texte_statut_propose_evaluation',
			'publie'   => 'texte_statut_publie',
			'refuse'   => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prop,prepa',
				//~ 'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'texte_changer_statut' => 'don:texte_changer_statut_don',
	);

	return $tables;
}

/**
 * Déclaration des tables secondaires (liaisons)
 *
 * @pipeline declarer_tables_auxiliaires
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function dons_declarer_tables_auxiliaires($tables) {
	$tables['spip_dons_campagnes_liens'] = array(
		'field' => array(
			'id_dons_campagne'   => 'bigint(21) DEFAULT "0" NOT NULL',
			'id_objet'           => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet'              => 'VARCHAR(25) DEFAULT "" NOT NULL',
			'vu'                 => 'VARCHAR(6) DEFAULT "non" NOT NULL',
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_dons_campagne,id_objet,objet',
			'KEY id_dons_campagne' => 'id_dons_campagne',
		)
	);

	return $tables;
}
